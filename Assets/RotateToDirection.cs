﻿using UnityEngine;
using System.Collections;

public class RotateToDirection : MonoBehaviour {

	public GameObject pos;
	// Use this for initialization
	void Start () {
		//Something added
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 velocity = transform.GetComponent<Rigidbody> ().velocity;

		if (velocity != new Vector3(0,0,0)) 
		{
			Vector3 direction = (velocity+transform.position);
			pos.transform.position = direction;

			transform.forward = direction;

			this.transform.rotation =  Quaternion.LookRotation(direction);
			//transform.LookAt (direction);
			//transform.rotation = Quaternion.LookRotation(direction);
		}
		}
}
