﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

	public float playerSpeed;

	private Vector3 velocity;
	private GameObject player;

	// Use this for initialization
	void Start () {
	
		player = this.gameObject;
		velocity = GetComponent<Rigidbody>().velocity;

	}
	
	// Update is called once per frame
	void Update () {

		GetComponent<Rigidbody>().velocity  = player.transform.forward * playerSpeed * Input.GetAxis ("Vertical") + player.transform.right * playerSpeed * Input.GetAxis ("Horizontal") ;
		GetComponent<Animator> ().SetFloat ("Speed", GetComponent<Rigidbody>().velocity.magnitude);

	}
}
