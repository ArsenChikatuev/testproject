//uScript Generated Code - Build 1.0.3036
//Generated with Debug Info
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class BallScript : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   delegate void ContinueExecution();
   ContinueExecution m_ContinueExecution;
   bool m_Breakpoint = false;
   const int MaxRelayCallCount = 1000;
   int relayCallCount = 0;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_1 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_ControlGameObjectMove logic_uScriptAct_ControlGameObjectMove_uScriptAct_ControlGameObjectMove_0 = new uScriptAct_ControlGameObjectMove( );
   UnityEngine.GameObject logic_uScriptAct_ControlGameObjectMove_Target_0 = default(UnityEngine.GameObject);
   uScriptAct_ControlGameObjectMove.Direction logic_uScriptAct_ControlGameObjectMove_moveDirection_0 = uScriptAct_ControlGameObjectMove.Direction.Up;
   System.Single logic_uScriptAct_ControlGameObjectMove_Speed_0 = (float) 0.04;
   System.Boolean logic_uScriptAct_ControlGameObjectMove_useLocal_0 = (bool) false;
   bool logic_uScriptAct_ControlGameObjectMove_Out_0 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_2 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == owner_Connection_1 || false == m_RegisteredForEvents )
      {
         owner_Connection_1 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_2 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_2 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_2 )
         {
            {
               uScript_Update component = event_UnityEngine_GameObject_Instance_2.GetComponent<uScript_Update>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_2.AddComponent<uScript_Update>();
               }
               if ( null != component )
               {
                  component.OnUpdate += Instance_OnUpdate_2;
                  component.OnLateUpdate += Instance_OnLateUpdate_2;
                  component.OnFixedUpdate += Instance_OnFixedUpdate_2;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != event_UnityEngine_GameObject_Instance_2 )
      {
         {
            uScript_Update component = event_UnityEngine_GameObject_Instance_2.GetComponent<uScript_Update>();
            if ( null != component )
            {
               component.OnUpdate -= Instance_OnUpdate_2;
               component.OnLateUpdate -= Instance_OnLateUpdate_2;
               component.OnFixedUpdate -= Instance_OnFixedUpdate_2;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_ControlGameObjectMove_uScriptAct_ControlGameObjectMove_0.SetParent(g);
      owner_Connection_1 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      //reset each Update, and increments each method call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      if ( null != m_ContinueExecution )
      {
         ContinueExecution continueEx = m_ContinueExecution;
         m_ContinueExecution = null;
         m_Breakpoint = false;
         continueEx( );
         return;
      }
      UpdateEditorValues( );
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnUpdate_2(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnUpdate_2( );
   }
   
   void Instance_OnLateUpdate_2(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnLateUpdate_2( );
   }
   
   void Instance_OnFixedUpdate_2(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnFixedUpdate_2( );
   }
   
   void Relay_In_0()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("72d6094f-7460-491f-b167-d0a85e453477", "Control_GameObject__Move_", Relay_In_0)) return; 
         {
            {
               logic_uScriptAct_ControlGameObjectMove_Target_0 = owner_Connection_1;
               
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_ControlGameObjectMove_uScriptAct_ControlGameObjectMove_0.In(logic_uScriptAct_ControlGameObjectMove_Target_0, logic_uScriptAct_ControlGameObjectMove_moveDirection_0, logic_uScriptAct_ControlGameObjectMove_Speed_0, logic_uScriptAct_ControlGameObjectMove_useLocal_0);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript BallScript.uscript at Control GameObject (Move).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnUpdate_2()
   {
      if (true == CheckDebugBreak("440bda29-7c58-4131-9901-f46a7a9015f5", "Global_Update", Relay_OnUpdate_2)) return; 
      Relay_In_0();
   }
   
   void Relay_OnLateUpdate_2()
   {
      if (true == CheckDebugBreak("440bda29-7c58-4131-9901-f46a7a9015f5", "Global_Update", Relay_OnLateUpdate_2)) return; 
   }
   
   void Relay_OnFixedUpdate_2()
   {
      if (true == CheckDebugBreak("440bda29-7c58-4131-9901-f46a7a9015f5", "Global_Update", Relay_OnFixedUpdate_2)) return; 
   }
   
   private void UpdateEditorValues( )
   {
   }
   bool CheckDebugBreak(string guid, string name, ContinueExecution method)
   {
      if (true == m_Breakpoint) return true;
      
      if (true == uScript_MasterComponent.FindBreakpoint(guid))
      {
         if (uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint == guid)
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = "";
         }
         else
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = guid;
            UpdateEditorValues( );
            UnityEngine.Debug.Log("uScript BREAK Node:" + name + " ((Time: " + Time.time + "");
            UnityEngine.Debug.Break();
            #if (!UNITY_FLASH)
            m_ContinueExecution = new ContinueExecution(method);
            #endif
            m_Breakpoint = true;
            return true;
         }
      }
      return false;
   }
}
