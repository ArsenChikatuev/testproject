//uScript Generated Code - Build 1.0.3036
//Generated with Debug Info
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class MainScript : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   delegate void ContinueExecution();
   ContinueExecution m_ContinueExecution;
   bool m_Breakpoint = false;
   const int MaxRelayCallCount = 1000;
   int relayCallCount = 0;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   UnityEngine.GameObject local_1_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_1_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_22_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_22_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_24_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_24_UnityEngine_GameObject_previous = null;
   System.String local_25_System_String = "isOpend";
   UnityEngine.Vector3 local_36_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.String local_AnimatorSpeed_System_String = "Speed";
   UnityEngine.GameObject local_CollideGO_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_CollideGO_UnityEngine_GameObject_previous = null;
   System.Boolean local_GateOpen_System_Boolean = (bool) false;
   System.String local_GroundTag_System_String = "Ground";
   System.Single local_H_Axis_System_Single = (float) 0;
   System.Boolean local_InGround_System_Boolean = (bool) false;
   System.Boolean local_IsJump_System_Boolean = (bool) false;
   System.Single local_MovementSpeed_System_Single = (float) 0;
   System.Single local_RotationSpeed_System_Single = (float) 0;
   System.Single local_V_Axis_System_Single = (float) 0;
   
   //owner nodes
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_4 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_4 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_4 = (float) 10;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_4;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_4;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_4 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_6 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_6 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_6 = (float) 10;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_6;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_6;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_6 = true;
   //pointer to script instanced logic node
   uScriptAct_AddForce logic_uScriptAct_AddForce_uScriptAct_AddForce_8 = new uScriptAct_AddForce( );
   UnityEngine.GameObject logic_uScriptAct_AddForce_Target_8 = default(UnityEngine.GameObject);
   UnityEngine.Vector3 logic_uScriptAct_AddForce_Force_8 = new Vector3( (float)0, (float)200, (float)0 );
   System.Single logic_uScriptAct_AddForce_Scale_8 = (float) 0;
   System.Boolean logic_uScriptAct_AddForce_UseForceMode_8 = (bool) false;
   UnityEngine.ForceMode logic_uScriptAct_AddForce_ForceModeType_8 = UnityEngine.ForceMode.Force;
   bool logic_uScriptAct_AddForce_Out_8 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_10 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_10 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_10 = true;
   bool logic_uScriptCon_CompareBool_False_10 = true;
   //pointer to script instanced logic node
   uScriptCon_GameObjectHasTag logic_uScriptCon_GameObjectHasTag_uScriptCon_GameObjectHasTag_13 = new uScriptCon_GameObjectHasTag( );
   UnityEngine.GameObject logic_uScriptCon_GameObjectHasTag_GameObject_13 = default(UnityEngine.GameObject);
   System.String[] logic_uScriptCon_GameObjectHasTag_Tag_13 = new System.String[] {};
   bool logic_uScriptCon_GameObjectHasTag_HasAllTags_13 = true;
   bool logic_uScriptCon_GameObjectHasTag_HasTag_13 = true;
   bool logic_uScriptCon_GameObjectHasTag_MissingTags_13 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_16 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_16;
   bool logic_uScriptAct_SetBool_Out_16 = true;
   bool logic_uScriptAct_SetBool_SetTrue_16 = true;
   bool logic_uScriptAct_SetBool_SetFalse_16 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_17 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_17 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_17 = true;
   bool logic_uScriptCon_CompareBool_False_17 = true;
   //pointer to script instanced logic node
   uScriptAct_AnimatorSetParameterBool logic_uScriptAct_AnimatorSetParameterBool_uScriptAct_AnimatorSetParameterBool_26 = new uScriptAct_AnimatorSetParameterBool( );
   UnityEngine.GameObject[] logic_uScriptAct_AnimatorSetParameterBool_Target_26 = new UnityEngine.GameObject[] {};
   System.String logic_uScriptAct_AnimatorSetParameterBool_Name_26 = "";
   System.Boolean logic_uScriptAct_AnimatorSetParameterBool_Value_26 = (bool) false;
   bool logic_uScriptAct_AnimatorSetParameterBool_Out_26 = true;
   //pointer to script instanced logic node
   uScriptAct_ToggleBool logic_uScriptAct_ToggleBool_uScriptAct_ToggleBool_27 = new uScriptAct_ToggleBool( );
   System.Boolean logic_uScriptAct_ToggleBool_Target_27 = (bool) false;
   System.Boolean logic_uScriptAct_ToggleBool_Result_27;
   bool logic_uScriptAct_ToggleBool_Out_27 = true;
   //pointer to script instanced logic node
   uScriptAct_AnimatorSetParameterFloat logic_uScriptAct_AnimatorSetParameterFloat_uScriptAct_AnimatorSetParameterFloat_30 = new uScriptAct_AnimatorSetParameterFloat( );
   UnityEngine.GameObject[] logic_uScriptAct_AnimatorSetParameterFloat_Target_30 = new UnityEngine.GameObject[] {};
   System.String logic_uScriptAct_AnimatorSetParameterFloat_Name_30 = "";
   System.Single logic_uScriptAct_AnimatorSetParameterFloat_Value_30 = (float) 0;
   bool logic_uScriptAct_AnimatorSetParameterFloat_Out_30 = true;
   //pointer to script instanced logic node
   uScriptAct_SetComponentsVector3 logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_34 = new uScriptAct_SetComponentsVector3( );
   System.Single logic_uScriptAct_SetComponentsVector3_X_34 = (float) 0;
   System.Single logic_uScriptAct_SetComponentsVector3_Y_34 = (float) 0;
   System.Single logic_uScriptAct_SetComponentsVector3_Z_34 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_SetComponentsVector3_OutputVector3_34;
   bool logic_uScriptAct_SetComponentsVector3_Out_34 = true;
   //pointer to script instanced logic node
   uScriptAct_SetRigidbodyVelocity logic_uScriptAct_SetRigidbodyVelocity_uScriptAct_SetRigidbodyVelocity_35 = new uScriptAct_SetRigidbodyVelocity( );
   UnityEngine.GameObject[] logic_uScriptAct_SetRigidbodyVelocity_Target_35 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetRigidbodyVelocity_Velocity_35 = new Vector3( );
   bool logic_uScriptAct_SetRigidbodyVelocity_Out_35 = true;
   
   //event nodes
   System.Single event_UnityEngine_GameObject_Horizontal_0 = (float) 0;
   System.Single event_UnityEngine_GameObject_Vertical_0 = (float) 0;
   System.Boolean event_UnityEngine_GameObject_Fire1_0 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_Fire2_0 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_Fire3_0 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_Jump_0 = (bool) false;
   System.Single event_UnityEngine_GameObject_MouseX_0 = (float) 0;
   System.Single event_UnityEngine_GameObject_MouseY_0 = (float) 0;
   System.Single event_UnityEngine_GameObject_MouseScrollWheel_0 = (float) 0;
   System.Single event_UnityEngine_GameObject_WindowShakeX_0 = (float) 0;
   System.Single event_UnityEngine_GameObject_WindowShakeY_0 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_0 = default(UnityEngine.GameObject);
   UnityEngine.Vector3 event_UnityEngine_GameObject_RelativeVelocity_12 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Rigidbody event_UnityEngine_GameObject_RigidBody_12 = default(UnityEngine.Rigidbody);
   UnityEngine.Collider event_UnityEngine_GameObject_Collider_12 = default(UnityEngine.Collider);
   UnityEngine.Transform event_UnityEngine_GameObject_Transform_12 = default(UnityEngine.Transform);
   UnityEngine.ContactPoint[] event_UnityEngine_GameObject_Contacts_12 = new UnityEngine.ContactPoint[ 0 ];
   UnityEngine.GameObject event_UnityEngine_GameObject_GameObject_12 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_GameObject_23 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_32 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_1_UnityEngine_GameObject = GameObject.Find( "Player" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_1_UnityEngine_GameObject_previous != local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_1_UnityEngine_GameObject_previous )
         {
            {
               uScript_Collision component = local_1_UnityEngine_GameObject_previous.GetComponent<uScript_Collision>();
               if ( null != component )
               {
                  component.OnEnterCollision -= Instance_OnEnterCollision_12;
                  component.OnExitCollision -= Instance_OnExitCollision_12;
                  component.WhileInsideCollision -= Instance_WhileInsideCollision_12;
               }
            }
         }
         
         local_1_UnityEngine_GameObject_previous = local_1_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_1_UnityEngine_GameObject )
         {
            {
               uScript_Collision component = local_1_UnityEngine_GameObject.GetComponent<uScript_Collision>();
               if ( null == component )
               {
                  component = local_1_UnityEngine_GameObject.AddComponent<uScript_Collision>();
               }
               if ( null != component )
               {
                  component.OnEnterCollision += Instance_OnEnterCollision_12;
                  component.OnExitCollision += Instance_OnExitCollision_12;
                  component.WhileInsideCollision += Instance_WhileInsideCollision_12;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_CollideGO_UnityEngine_GameObject_previous != local_CollideGO_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_CollideGO_UnityEngine_GameObject_previous = local_CollideGO_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_22_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_22_UnityEngine_GameObject = GameObject.Find( "Button" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_22_UnityEngine_GameObject_previous != local_22_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_22_UnityEngine_GameObject_previous )
         {
            {
               uScript_Trigger component = local_22_UnityEngine_GameObject_previous.GetComponent<uScript_Trigger>();
               if ( null != component )
               {
                  component.OnEnterTrigger -= Instance_OnEnterTrigger_23;
                  component.OnExitTrigger -= Instance_OnExitTrigger_23;
                  component.WhileInsideTrigger -= Instance_WhileInsideTrigger_23;
               }
            }
         }
         
         local_22_UnityEngine_GameObject_previous = local_22_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_22_UnityEngine_GameObject )
         {
            {
               uScript_Trigger component = local_22_UnityEngine_GameObject.GetComponent<uScript_Trigger>();
               if ( null == component )
               {
                  component = local_22_UnityEngine_GameObject.AddComponent<uScript_Trigger>();
               }
               if ( null != component )
               {
                  component.OnEnterTrigger += Instance_OnEnterTrigger_23;
                  component.OnExitTrigger += Instance_OnExitTrigger_23;
                  component.WhileInsideTrigger += Instance_WhileInsideTrigger_23;
               }
            }
         }
      }
      if ( null == local_24_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_24_UnityEngine_GameObject = GameObject.Find( "Gates" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_24_UnityEngine_GameObject_previous != local_24_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_24_UnityEngine_GameObject_previous = local_24_UnityEngine_GameObject;
         
         //setup new listeners
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_1_UnityEngine_GameObject_previous != local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_1_UnityEngine_GameObject_previous )
         {
            {
               uScript_Collision component = local_1_UnityEngine_GameObject_previous.GetComponent<uScript_Collision>();
               if ( null != component )
               {
                  component.OnEnterCollision -= Instance_OnEnterCollision_12;
                  component.OnExitCollision -= Instance_OnExitCollision_12;
                  component.WhileInsideCollision -= Instance_WhileInsideCollision_12;
               }
            }
         }
         
         local_1_UnityEngine_GameObject_previous = local_1_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_1_UnityEngine_GameObject )
         {
            {
               uScript_Collision component = local_1_UnityEngine_GameObject.GetComponent<uScript_Collision>();
               if ( null == component )
               {
                  component = local_1_UnityEngine_GameObject.AddComponent<uScript_Collision>();
               }
               if ( null != component )
               {
                  component.OnEnterCollision += Instance_OnEnterCollision_12;
                  component.OnExitCollision += Instance_OnExitCollision_12;
                  component.WhileInsideCollision += Instance_WhileInsideCollision_12;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_CollideGO_UnityEngine_GameObject_previous != local_CollideGO_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_CollideGO_UnityEngine_GameObject_previous = local_CollideGO_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_22_UnityEngine_GameObject_previous != local_22_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_22_UnityEngine_GameObject_previous )
         {
            {
               uScript_Trigger component = local_22_UnityEngine_GameObject_previous.GetComponent<uScript_Trigger>();
               if ( null != component )
               {
                  component.OnEnterTrigger -= Instance_OnEnterTrigger_23;
                  component.OnExitTrigger -= Instance_OnExitTrigger_23;
                  component.WhileInsideTrigger -= Instance_WhileInsideTrigger_23;
               }
            }
         }
         
         local_22_UnityEngine_GameObject_previous = local_22_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_22_UnityEngine_GameObject )
         {
            {
               uScript_Trigger component = local_22_UnityEngine_GameObject.GetComponent<uScript_Trigger>();
               if ( null == component )
               {
                  component = local_22_UnityEngine_GameObject.AddComponent<uScript_Trigger>();
               }
               if ( null != component )
               {
                  component.OnEnterTrigger += Instance_OnEnterTrigger_23;
                  component.OnExitTrigger += Instance_OnExitTrigger_23;
                  component.WhileInsideTrigger += Instance_WhileInsideTrigger_23;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_24_UnityEngine_GameObject_previous != local_24_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_24_UnityEngine_GameObject_previous = local_24_UnityEngine_GameObject;
         
         //setup new listeners
      }
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_0 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_0 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_0 )
         {
            {
               uScript_ManagedInput component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_ManagedInput>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_0.AddComponent<uScript_ManagedInput>();
               }
               if ( null != component )
               {
                  component.OnInputEvent += Instance_OnInputEvent_0;
               }
            }
         }
      }
      if ( null == event_UnityEngine_GameObject_Instance_32 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_32 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_32 )
         {
            {
               uScript_Update component = event_UnityEngine_GameObject_Instance_32.GetComponent<uScript_Update>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_32.AddComponent<uScript_Update>();
               }
               if ( null != component )
               {
                  component.OnUpdate += Instance_OnUpdate_32;
                  component.OnLateUpdate += Instance_OnLateUpdate_32;
                  component.OnFixedUpdate += Instance_OnFixedUpdate_32;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != local_1_UnityEngine_GameObject )
      {
         {
            uScript_Collision component = local_1_UnityEngine_GameObject.GetComponent<uScript_Collision>();
            if ( null != component )
            {
               component.OnEnterCollision -= Instance_OnEnterCollision_12;
               component.OnExitCollision -= Instance_OnExitCollision_12;
               component.WhileInsideCollision -= Instance_WhileInsideCollision_12;
            }
         }
      }
      if ( null != local_22_UnityEngine_GameObject )
      {
         {
            uScript_Trigger component = local_22_UnityEngine_GameObject.GetComponent<uScript_Trigger>();
            if ( null != component )
            {
               component.OnEnterTrigger -= Instance_OnEnterTrigger_23;
               component.OnExitTrigger -= Instance_OnExitTrigger_23;
               component.WhileInsideTrigger -= Instance_WhileInsideTrigger_23;
            }
         }
      }
      if ( null != event_UnityEngine_GameObject_Instance_0 )
      {
         {
            uScript_ManagedInput component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_ManagedInput>();
            if ( null != component )
            {
               component.OnInputEvent -= Instance_OnInputEvent_0;
            }
         }
      }
      if ( null != event_UnityEngine_GameObject_Instance_32 )
      {
         {
            uScript_Update component = event_UnityEngine_GameObject_Instance_32.GetComponent<uScript_Update>();
            if ( null != component )
            {
               component.OnUpdate -= Instance_OnUpdate_32;
               component.OnLateUpdate -= Instance_OnLateUpdate_32;
               component.OnFixedUpdate -= Instance_OnFixedUpdate_32;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_4.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_6.SetParent(g);
      logic_uScriptAct_AddForce_uScriptAct_AddForce_8.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_10.SetParent(g);
      logic_uScriptCon_GameObjectHasTag_uScriptCon_GameObjectHasTag_13.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_16.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_17.SetParent(g);
      logic_uScriptAct_AnimatorSetParameterBool_uScriptAct_AnimatorSetParameterBool_26.SetParent(g);
      logic_uScriptAct_ToggleBool_uScriptAct_ToggleBool_27.SetParent(g);
      logic_uScriptAct_AnimatorSetParameterFloat_uScriptAct_AnimatorSetParameterFloat_30.SetParent(g);
      logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_34.SetParent(g);
      logic_uScriptAct_SetRigidbodyVelocity_uScriptAct_SetRigidbodyVelocity_35.SetParent(g);
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      //reset each Update, and increments each method call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      if ( null != m_ContinueExecution )
      {
         ContinueExecution continueEx = m_ContinueExecution;
         m_ContinueExecution = null;
         m_Breakpoint = false;
         continueEx( );
         return;
      }
      UpdateEditorValues( );
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnInputEvent_0(object o, uScript_ManagedInput.CustomEventBoolArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      event_UnityEngine_GameObject_Horizontal_0 = e.Horizontal;
      event_UnityEngine_GameObject_Vertical_0 = e.Vertical;
      event_UnityEngine_GameObject_Fire1_0 = e.Fire1;
      event_UnityEngine_GameObject_Fire2_0 = e.Fire2;
      event_UnityEngine_GameObject_Fire3_0 = e.Fire3;
      event_UnityEngine_GameObject_Jump_0 = e.Jump;
      event_UnityEngine_GameObject_MouseX_0 = e.MouseX;
      event_UnityEngine_GameObject_MouseY_0 = e.MouseY;
      event_UnityEngine_GameObject_MouseScrollWheel_0 = e.MouseScrollWheel;
      event_UnityEngine_GameObject_WindowShakeX_0 = e.WindowShakeX;
      event_UnityEngine_GameObject_WindowShakeY_0 = e.WindowShakeY;
      //relay event to nodes
      Relay_OnInputEvent_0( );
   }
   
   void Instance_OnEnterCollision_12(object o, uScript_Collision.CollisionEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      event_UnityEngine_GameObject_RelativeVelocity_12 = e.RelativeVelocity;
      event_UnityEngine_GameObject_RigidBody_12 = e.RigidBody;
      event_UnityEngine_GameObject_Collider_12 = e.Collider;
      event_UnityEngine_GameObject_Transform_12 = e.Transform;
      event_UnityEngine_GameObject_Contacts_12 = e.Contacts;
      event_UnityEngine_GameObject_GameObject_12 = e.GameObject;
      //relay event to nodes
      Relay_OnEnterCollision_12( );
   }
   
   void Instance_OnExitCollision_12(object o, uScript_Collision.CollisionEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      event_UnityEngine_GameObject_RelativeVelocity_12 = e.RelativeVelocity;
      event_UnityEngine_GameObject_RigidBody_12 = e.RigidBody;
      event_UnityEngine_GameObject_Collider_12 = e.Collider;
      event_UnityEngine_GameObject_Transform_12 = e.Transform;
      event_UnityEngine_GameObject_Contacts_12 = e.Contacts;
      event_UnityEngine_GameObject_GameObject_12 = e.GameObject;
      //relay event to nodes
      Relay_OnExitCollision_12( );
   }
   
   void Instance_WhileInsideCollision_12(object o, uScript_Collision.CollisionEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      event_UnityEngine_GameObject_RelativeVelocity_12 = e.RelativeVelocity;
      event_UnityEngine_GameObject_RigidBody_12 = e.RigidBody;
      event_UnityEngine_GameObject_Collider_12 = e.Collider;
      event_UnityEngine_GameObject_Transform_12 = e.Transform;
      event_UnityEngine_GameObject_Contacts_12 = e.Contacts;
      event_UnityEngine_GameObject_GameObject_12 = e.GameObject;
      //relay event to nodes
      Relay_WhileInsideCollision_12( );
   }
   
   void Instance_OnEnterTrigger_23(object o, uScript_Trigger.TriggerEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      event_UnityEngine_GameObject_GameObject_23 = e.GameObject;
      //relay event to nodes
      Relay_OnEnterTrigger_23( );
   }
   
   void Instance_OnExitTrigger_23(object o, uScript_Trigger.TriggerEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      event_UnityEngine_GameObject_GameObject_23 = e.GameObject;
      //relay event to nodes
      Relay_OnExitTrigger_23( );
   }
   
   void Instance_WhileInsideTrigger_23(object o, uScript_Trigger.TriggerEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      event_UnityEngine_GameObject_GameObject_23 = e.GameObject;
      //relay event to nodes
      Relay_WhileInsideTrigger_23( );
   }
   
   void Instance_OnUpdate_32(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnUpdate_32( );
   }
   
   void Instance_OnLateUpdate_32(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnLateUpdate_32( );
   }
   
   void Instance_OnFixedUpdate_32(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnFixedUpdate_32( );
   }
   
   void Relay_OnInputEvent_0()
   {
      if (true == CheckDebugBreak("ae9f1b6f-7d74-4ed1-9a44-a454f726c2ab", "Managed_Input_Events", Relay_OnInputEvent_0)) return; 
      local_H_Axis_System_Single = event_UnityEngine_GameObject_Horizontal_0;
      local_V_Axis_System_Single = event_UnityEngine_GameObject_Vertical_0;
      local_IsJump_System_Boolean = event_UnityEngine_GameObject_Jump_0;
      Relay_In_4();
      Relay_In_10();
      Relay_In_6();
   }
   
   void Relay_In_4()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("7c8dca8d-c4b5-496f-940c-ff2e15ec0d9b", "Multiply_Float", Relay_In_4)) return; 
         {
            {
               logic_uScriptAct_MultiplyFloat_v2_A_4 = local_V_Axis_System_Single;
               
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_4.In(logic_uScriptAct_MultiplyFloat_v2_A_4, logic_uScriptAct_MultiplyFloat_v2_B_4, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_4, out logic_uScriptAct_MultiplyFloat_v2_IntResult_4);
         local_MovementSpeed_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_4;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript MainScript.uscript at Multiply Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_6()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("6e39b39d-082d-4576-ab65-613326bf1693", "Multiply_Float", Relay_In_6)) return; 
         {
            {
               logic_uScriptAct_MultiplyFloat_v2_A_6 = local_H_Axis_System_Single;
               
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_6.In(logic_uScriptAct_MultiplyFloat_v2_A_6, logic_uScriptAct_MultiplyFloat_v2_B_6, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_6, out logic_uScriptAct_MultiplyFloat_v2_IntResult_6);
         local_RotationSpeed_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_6;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript MainScript.uscript at Multiply Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_8()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("48a441f9-5c42-47d8-b305-1c1f4cddad84", "Add_Force", Relay_In_8)) return; 
         {
            {
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_1_UnityEngine_GameObject_previous != local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     if ( null != local_1_UnityEngine_GameObject_previous )
                     {
                        {
                           uScript_Collision component = local_1_UnityEngine_GameObject_previous.GetComponent<uScript_Collision>();
                           if ( null != component )
                           {
                              component.OnEnterCollision -= Instance_OnEnterCollision_12;
                              component.OnExitCollision -= Instance_OnExitCollision_12;
                              component.WhileInsideCollision -= Instance_WhileInsideCollision_12;
                           }
                        }
                     }
                     
                     local_1_UnityEngine_GameObject_previous = local_1_UnityEngine_GameObject;
                     
                     //setup new listeners
                     if ( null != local_1_UnityEngine_GameObject )
                     {
                        {
                           uScript_Collision component = local_1_UnityEngine_GameObject.GetComponent<uScript_Collision>();
                           if ( null == component )
                           {
                              component = local_1_UnityEngine_GameObject.AddComponent<uScript_Collision>();
                           }
                           if ( null != component )
                           {
                              component.OnEnterCollision += Instance_OnEnterCollision_12;
                              component.OnExitCollision += Instance_OnExitCollision_12;
                              component.WhileInsideCollision += Instance_WhileInsideCollision_12;
                           }
                        }
                     }
                  }
               }
               logic_uScriptAct_AddForce_Target_8 = local_1_UnityEngine_GameObject;
               
            }
            {
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_AddForce_uScriptAct_AddForce_8.In(logic_uScriptAct_AddForce_Target_8, logic_uScriptAct_AddForce_Force_8, logic_uScriptAct_AddForce_Scale_8, logic_uScriptAct_AddForce_UseForceMode_8, logic_uScriptAct_AddForce_ForceModeType_8);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript MainScript.uscript at Add Force.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_10()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("360f0278-7128-4454-8cfe-78d05896c597", "Compare_Bool", Relay_In_10)) return; 
         {
            {
               logic_uScriptCon_CompareBool_Bool_10 = local_IsJump_System_Boolean;
               
            }
         }
         logic_uScriptCon_CompareBool_uScriptCon_CompareBool_10.In(logic_uScriptCon_CompareBool_Bool_10);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_10.True;
         
         if ( test_0 == true )
         {
            Relay_In_17();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript MainScript.uscript at Compare Bool.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnEnterCollision_12()
   {
      if (true == CheckDebugBreak("bbf3da06-6efc-4300-80f1-f582cc291c07", "On_Collision", Relay_OnEnterCollision_12)) return; 
      local_CollideGO_UnityEngine_GameObject = event_UnityEngine_GameObject_GameObject_12;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_CollideGO_UnityEngine_GameObject_previous != local_CollideGO_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_CollideGO_UnityEngine_GameObject_previous = local_CollideGO_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
   }
   
   void Relay_OnExitCollision_12()
   {
      if (true == CheckDebugBreak("bbf3da06-6efc-4300-80f1-f582cc291c07", "On_Collision", Relay_OnExitCollision_12)) return; 
      local_CollideGO_UnityEngine_GameObject = event_UnityEngine_GameObject_GameObject_12;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_CollideGO_UnityEngine_GameObject_previous != local_CollideGO_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_CollideGO_UnityEngine_GameObject_previous = local_CollideGO_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_False_16();
   }
   
   void Relay_WhileInsideCollision_12()
   {
      if (true == CheckDebugBreak("bbf3da06-6efc-4300-80f1-f582cc291c07", "On_Collision", Relay_WhileInsideCollision_12)) return; 
      local_CollideGO_UnityEngine_GameObject = event_UnityEngine_GameObject_GameObject_12;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_CollideGO_UnityEngine_GameObject_previous != local_CollideGO_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_CollideGO_UnityEngine_GameObject_previous = local_CollideGO_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_In_13();
   }
   
   void Relay_In_13()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("6bb64dc7-439d-4d32-b2b1-825befade04c", "GameObject_Has_Tag", Relay_In_13)) return; 
         {
            {
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_CollideGO_UnityEngine_GameObject_previous != local_CollideGO_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_CollideGO_UnityEngine_GameObject_previous = local_CollideGO_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               logic_uScriptCon_GameObjectHasTag_GameObject_13 = local_CollideGO_UnityEngine_GameObject;
               
            }
            {
               int index = 0;
               if ( logic_uScriptCon_GameObjectHasTag_Tag_13.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptCon_GameObjectHasTag_Tag_13, index + 1);
               }
               logic_uScriptCon_GameObjectHasTag_Tag_13[ index++ ] = local_GroundTag_System_String;
               
            }
         }
         logic_uScriptCon_GameObjectHasTag_uScriptCon_GameObjectHasTag_13.In(logic_uScriptCon_GameObjectHasTag_GameObject_13, logic_uScriptCon_GameObjectHasTag_Tag_13);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptCon_GameObjectHasTag_uScriptCon_GameObjectHasTag_13.HasTag;
         
         if ( test_0 == true )
         {
            Relay_True_16();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript MainScript.uscript at GameObject Has Tag.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_True_16()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("31c4a930-1695-4cf8-b23d-2d5c2901275f", "Set_Bool", Relay_True_16)) return; 
         {
            {
            }
         }
         logic_uScriptAct_SetBool_uScriptAct_SetBool_16.True(out logic_uScriptAct_SetBool_Target_16);
         local_InGround_System_Boolean = logic_uScriptAct_SetBool_Target_16;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript MainScript.uscript at Set Bool.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_False_16()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("31c4a930-1695-4cf8-b23d-2d5c2901275f", "Set_Bool", Relay_False_16)) return; 
         {
            {
            }
         }
         logic_uScriptAct_SetBool_uScriptAct_SetBool_16.False(out logic_uScriptAct_SetBool_Target_16);
         local_InGround_System_Boolean = logic_uScriptAct_SetBool_Target_16;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript MainScript.uscript at Set Bool.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_17()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("d5013d8b-a84f-46a5-a62c-6c2b69b6b8b7", "Compare_Bool", Relay_In_17)) return; 
         {
            {
               logic_uScriptCon_CompareBool_Bool_17 = local_InGround_System_Boolean;
               
            }
         }
         logic_uScriptCon_CompareBool_uScriptCon_CompareBool_17.In(logic_uScriptCon_CompareBool_Bool_17);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_17.True;
         
         if ( test_0 == true )
         {
            Relay_In_8();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript MainScript.uscript at Compare Bool.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnEnterTrigger_23()
   {
      if (true == CheckDebugBreak("545b67ff-9623-46b4-8050-1cf3f58dc5ad", "Trigger_Event", Relay_OnEnterTrigger_23)) return; 
      local_1_UnityEngine_GameObject = event_UnityEngine_GameObject_GameObject_23;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_1_UnityEngine_GameObject_previous != local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            if ( null != local_1_UnityEngine_GameObject_previous )
            {
               {
                  uScript_Collision component = local_1_UnityEngine_GameObject_previous.GetComponent<uScript_Collision>();
                  if ( null != component )
                  {
                     component.OnEnterCollision -= Instance_OnEnterCollision_12;
                     component.OnExitCollision -= Instance_OnExitCollision_12;
                     component.WhileInsideCollision -= Instance_WhileInsideCollision_12;
                  }
               }
            }
            
            local_1_UnityEngine_GameObject_previous = local_1_UnityEngine_GameObject;
            
            //setup new listeners
            if ( null != local_1_UnityEngine_GameObject )
            {
               {
                  uScript_Collision component = local_1_UnityEngine_GameObject.GetComponent<uScript_Collision>();
                  if ( null == component )
                  {
                     component = local_1_UnityEngine_GameObject.AddComponent<uScript_Collision>();
                  }
                  if ( null != component )
                  {
                     component.OnEnterCollision += Instance_OnEnterCollision_12;
                     component.OnExitCollision += Instance_OnExitCollision_12;
                     component.WhileInsideCollision += Instance_WhileInsideCollision_12;
                  }
               }
            }
         }
      }
      Relay_In_26();
   }
   
   void Relay_OnExitTrigger_23()
   {
      if (true == CheckDebugBreak("545b67ff-9623-46b4-8050-1cf3f58dc5ad", "Trigger_Event", Relay_OnExitTrigger_23)) return; 
      local_1_UnityEngine_GameObject = event_UnityEngine_GameObject_GameObject_23;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_1_UnityEngine_GameObject_previous != local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            if ( null != local_1_UnityEngine_GameObject_previous )
            {
               {
                  uScript_Collision component = local_1_UnityEngine_GameObject_previous.GetComponent<uScript_Collision>();
                  if ( null != component )
                  {
                     component.OnEnterCollision -= Instance_OnEnterCollision_12;
                     component.OnExitCollision -= Instance_OnExitCollision_12;
                     component.WhileInsideCollision -= Instance_WhileInsideCollision_12;
                  }
               }
            }
            
            local_1_UnityEngine_GameObject_previous = local_1_UnityEngine_GameObject;
            
            //setup new listeners
            if ( null != local_1_UnityEngine_GameObject )
            {
               {
                  uScript_Collision component = local_1_UnityEngine_GameObject.GetComponent<uScript_Collision>();
                  if ( null == component )
                  {
                     component = local_1_UnityEngine_GameObject.AddComponent<uScript_Collision>();
                  }
                  if ( null != component )
                  {
                     component.OnEnterCollision += Instance_OnEnterCollision_12;
                     component.OnExitCollision += Instance_OnExitCollision_12;
                     component.WhileInsideCollision += Instance_WhileInsideCollision_12;
                  }
               }
            }
         }
      }
   }
   
   void Relay_WhileInsideTrigger_23()
   {
      if (true == CheckDebugBreak("545b67ff-9623-46b4-8050-1cf3f58dc5ad", "Trigger_Event", Relay_WhileInsideTrigger_23)) return; 
      local_1_UnityEngine_GameObject = event_UnityEngine_GameObject_GameObject_23;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_1_UnityEngine_GameObject_previous != local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            if ( null != local_1_UnityEngine_GameObject_previous )
            {
               {
                  uScript_Collision component = local_1_UnityEngine_GameObject_previous.GetComponent<uScript_Collision>();
                  if ( null != component )
                  {
                     component.OnEnterCollision -= Instance_OnEnterCollision_12;
                     component.OnExitCollision -= Instance_OnExitCollision_12;
                     component.WhileInsideCollision -= Instance_WhileInsideCollision_12;
                  }
               }
            }
            
            local_1_UnityEngine_GameObject_previous = local_1_UnityEngine_GameObject;
            
            //setup new listeners
            if ( null != local_1_UnityEngine_GameObject )
            {
               {
                  uScript_Collision component = local_1_UnityEngine_GameObject.GetComponent<uScript_Collision>();
                  if ( null == component )
                  {
                     component = local_1_UnityEngine_GameObject.AddComponent<uScript_Collision>();
                  }
                  if ( null != component )
                  {
                     component.OnEnterCollision += Instance_OnEnterCollision_12;
                     component.OnExitCollision += Instance_OnExitCollision_12;
                     component.WhileInsideCollision += Instance_WhileInsideCollision_12;
                  }
               }
            }
         }
      }
   }
   
   void Relay_In_26()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("c91ca9d6-c11a-4254-99e3-a0f8054a744e", "Set_Animator_Parameter__Bool_", Relay_In_26)) return; 
         {
            {
               int index = 0;
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_24_UnityEngine_GameObject_previous != local_24_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_24_UnityEngine_GameObject_previous = local_24_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               if ( logic_uScriptAct_AnimatorSetParameterBool_Target_26.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_AnimatorSetParameterBool_Target_26, index + 1);
               }
               logic_uScriptAct_AnimatorSetParameterBool_Target_26[ index++ ] = local_24_UnityEngine_GameObject;
               
            }
            {
               logic_uScriptAct_AnimatorSetParameterBool_Name_26 = local_25_System_String;
               
            }
            {
               logic_uScriptAct_AnimatorSetParameterBool_Value_26 = local_GateOpen_System_Boolean;
               
            }
         }
         logic_uScriptAct_AnimatorSetParameterBool_uScriptAct_AnimatorSetParameterBool_26.In(logic_uScriptAct_AnimatorSetParameterBool_Target_26, logic_uScriptAct_AnimatorSetParameterBool_Name_26, logic_uScriptAct_AnimatorSetParameterBool_Value_26);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_AnimatorSetParameterBool_uScriptAct_AnimatorSetParameterBool_26.Out;
         
         if ( test_0 == true )
         {
            Relay_Toggle_27();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript MainScript.uscript at Set Animator Parameter (Bool).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Toggle_27()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("f6534d04-b59c-47eb-a39f-3811dd2dfad3", "Toggle_Bool", Relay_Toggle_27)) return; 
         {
            {
               logic_uScriptAct_ToggleBool_Target_27 = local_GateOpen_System_Boolean;
               
            }
            {
            }
         }
         logic_uScriptAct_ToggleBool_uScriptAct_ToggleBool_27.Toggle(logic_uScriptAct_ToggleBool_Target_27, out logic_uScriptAct_ToggleBool_Result_27);
         local_GateOpen_System_Boolean = logic_uScriptAct_ToggleBool_Result_27;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript MainScript.uscript at Toggle Bool.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_30()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("cab46727-496f-46dc-aec9-30d40e87df90", "Set_Animator_Parameter__Float_", Relay_In_30)) return; 
         {
            {
               int index = 0;
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_1_UnityEngine_GameObject_previous != local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     if ( null != local_1_UnityEngine_GameObject_previous )
                     {
                        {
                           uScript_Collision component = local_1_UnityEngine_GameObject_previous.GetComponent<uScript_Collision>();
                           if ( null != component )
                           {
                              component.OnEnterCollision -= Instance_OnEnterCollision_12;
                              component.OnExitCollision -= Instance_OnExitCollision_12;
                              component.WhileInsideCollision -= Instance_WhileInsideCollision_12;
                           }
                        }
                     }
                     
                     local_1_UnityEngine_GameObject_previous = local_1_UnityEngine_GameObject;
                     
                     //setup new listeners
                     if ( null != local_1_UnityEngine_GameObject )
                     {
                        {
                           uScript_Collision component = local_1_UnityEngine_GameObject.GetComponent<uScript_Collision>();
                           if ( null == component )
                           {
                              component = local_1_UnityEngine_GameObject.AddComponent<uScript_Collision>();
                           }
                           if ( null != component )
                           {
                              component.OnEnterCollision += Instance_OnEnterCollision_12;
                              component.OnExitCollision += Instance_OnExitCollision_12;
                              component.WhileInsideCollision += Instance_WhileInsideCollision_12;
                           }
                        }
                     }
                  }
               }
               if ( logic_uScriptAct_AnimatorSetParameterFloat_Target_30.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_AnimatorSetParameterFloat_Target_30, index + 1);
               }
               logic_uScriptAct_AnimatorSetParameterFloat_Target_30[ index++ ] = local_1_UnityEngine_GameObject;
               
            }
            {
               logic_uScriptAct_AnimatorSetParameterFloat_Name_30 = local_AnimatorSpeed_System_String;
               
            }
            {
               logic_uScriptAct_AnimatorSetParameterFloat_Value_30 = local_V_Axis_System_Single;
               
            }
         }
         logic_uScriptAct_AnimatorSetParameterFloat_uScriptAct_AnimatorSetParameterFloat_30.In(logic_uScriptAct_AnimatorSetParameterFloat_Target_30, logic_uScriptAct_AnimatorSetParameterFloat_Name_30, logic_uScriptAct_AnimatorSetParameterFloat_Value_30);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript MainScript.uscript at Set Animator Parameter (Float).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnUpdate_32()
   {
      if (true == CheckDebugBreak("52ce48e4-9ddb-44e0-8ef7-a3c7fc43d5a6", "Global_Update", Relay_OnUpdate_32)) return; 
      Relay_In_30();
      Relay_In_35();
      Relay_In_34();
   }
   
   void Relay_OnLateUpdate_32()
   {
      if (true == CheckDebugBreak("52ce48e4-9ddb-44e0-8ef7-a3c7fc43d5a6", "Global_Update", Relay_OnLateUpdate_32)) return; 
   }
   
   void Relay_OnFixedUpdate_32()
   {
      if (true == CheckDebugBreak("52ce48e4-9ddb-44e0-8ef7-a3c7fc43d5a6", "Global_Update", Relay_OnFixedUpdate_32)) return; 
   }
   
   void Relay_In_34()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("905bd7d5-f802-44a1-9b2e-5ab53b1ba9b1", "Set_Components__Vector3_", Relay_In_34)) return; 
         {
            {
               logic_uScriptAct_SetComponentsVector3_X_34 = local_RotationSpeed_System_Single;
               
            }
            {
            }
            {
               logic_uScriptAct_SetComponentsVector3_Z_34 = local_MovementSpeed_System_Single;
               
            }
            {
            }
         }
         logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_34.In(logic_uScriptAct_SetComponentsVector3_X_34, logic_uScriptAct_SetComponentsVector3_Y_34, logic_uScriptAct_SetComponentsVector3_Z_34, out logic_uScriptAct_SetComponentsVector3_OutputVector3_34);
         local_36_UnityEngine_Vector3 = logic_uScriptAct_SetComponentsVector3_OutputVector3_34;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript MainScript.uscript at Set Components (Vector3).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_35()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("c1e735c2-97cb-4fe4-8715-a1f132f9af3d", "Set_Rigidbody_Velocity", Relay_In_35)) return; 
         {
            {
               int index = 0;
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_1_UnityEngine_GameObject_previous != local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     if ( null != local_1_UnityEngine_GameObject_previous )
                     {
                        {
                           uScript_Collision component = local_1_UnityEngine_GameObject_previous.GetComponent<uScript_Collision>();
                           if ( null != component )
                           {
                              component.OnEnterCollision -= Instance_OnEnterCollision_12;
                              component.OnExitCollision -= Instance_OnExitCollision_12;
                              component.WhileInsideCollision -= Instance_WhileInsideCollision_12;
                           }
                        }
                     }
                     
                     local_1_UnityEngine_GameObject_previous = local_1_UnityEngine_GameObject;
                     
                     //setup new listeners
                     if ( null != local_1_UnityEngine_GameObject )
                     {
                        {
                           uScript_Collision component = local_1_UnityEngine_GameObject.GetComponent<uScript_Collision>();
                           if ( null == component )
                           {
                              component = local_1_UnityEngine_GameObject.AddComponent<uScript_Collision>();
                           }
                           if ( null != component )
                           {
                              component.OnEnterCollision += Instance_OnEnterCollision_12;
                              component.OnExitCollision += Instance_OnExitCollision_12;
                              component.WhileInsideCollision += Instance_WhileInsideCollision_12;
                           }
                        }
                     }
                  }
               }
               if ( logic_uScriptAct_SetRigidbodyVelocity_Target_35.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_SetRigidbodyVelocity_Target_35, index + 1);
               }
               logic_uScriptAct_SetRigidbodyVelocity_Target_35[ index++ ] = local_1_UnityEngine_GameObject;
               
            }
            {
               logic_uScriptAct_SetRigidbodyVelocity_Velocity_35 = local_36_UnityEngine_Vector3;
               
            }
         }
         logic_uScriptAct_SetRigidbodyVelocity_uScriptAct_SetRigidbodyVelocity_35.In(logic_uScriptAct_SetRigidbodyVelocity_Target_35, logic_uScriptAct_SetRigidbodyVelocity_Velocity_35);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript MainScript.uscript at Set Rigidbody Velocity.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   private void UpdateEditorValues( )
   {
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "MainScript.uscript:1", local_1_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "37b26dca-c848-4d3b-8e91-5f73bf4f5d06", local_1_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "MainScript.uscript:V-Axis", local_V_Axis_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "859e4346-7cef-4ed0-972e-5e623bcb314a", local_V_Axis_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "MainScript.uscript:H-Axis", local_H_Axis_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "76055100-11b5-4496-8bba-5bb2eb0201e2", local_H_Axis_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "MainScript.uscript:MovementSpeed", local_MovementSpeed_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "349fd34a-68d1-4e98-a0fa-05b359c6091f", local_MovementSpeed_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "MainScript.uscript:RotationSpeed", local_RotationSpeed_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "8448e2b4-9faa-457b-90b6-8135bc6db60c", local_RotationSpeed_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "MainScript.uscript:IsJump", local_IsJump_System_Boolean);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "4b6528bc-78e9-4724-858d-cc7a51d50286", local_IsJump_System_Boolean);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "MainScript.uscript:InGround", local_InGround_System_Boolean);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "2e1c9a12-d334-4701-a1e2-b7707981b2da", local_InGround_System_Boolean);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "MainScript.uscript:CollideGO", local_CollideGO_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "11228776-c07f-43e8-bb32-773a1d5c8f28", local_CollideGO_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "MainScript.uscript:GroundTag", local_GroundTag_System_String);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "f4c4c869-a364-49bf-b214-e36d4bfd63d5", local_GroundTag_System_String);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "MainScript.uscript:22", local_22_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "212845e6-dc03-41d1-b0cd-8fe5be738b01", local_22_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "MainScript.uscript:24", local_24_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "16139106-e302-4902-ad01-5a9eeb796cbc", local_24_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "MainScript.uscript:25", local_25_System_String);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "86a9148b-9208-4df0-b36e-70248c25d4dd", local_25_System_String);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "MainScript.uscript:GateOpen", local_GateOpen_System_Boolean);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "5c530dc5-931a-4bb3-9ae6-86ff7e749ff7", local_GateOpen_System_Boolean);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "MainScript.uscript:AnimatorSpeed", local_AnimatorSpeed_System_String);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "ba1f2e51-9198-487b-a632-e121891e3f13", local_AnimatorSpeed_System_String);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "MainScript.uscript:36", local_36_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "3d23bd84-a562-45cf-8263-154e7b950106", local_36_UnityEngine_Vector3);
   }
   bool CheckDebugBreak(string guid, string name, ContinueExecution method)
   {
      if (true == m_Breakpoint) return true;
      
      if (true == uScript_MasterComponent.FindBreakpoint(guid))
      {
         if (uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint == guid)
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = "";
         }
         else
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = guid;
            UpdateEditorValues( );
            UnityEngine.Debug.Log("uScript BREAK Node:" + name + " ((Time: " + Time.time + "");
            UnityEngine.Debug.Break();
            #if (!UNITY_FLASH)
            m_ContinueExecution = new ContinueExecution(method);
            #endif
            m_Breakpoint = true;
            return true;
         }
      }
      return false;
   }
}
