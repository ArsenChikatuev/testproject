﻿using UnityEngine;
using System.Collections;

[NodePath("Actions/Variables/Lists/GameObject")]

[NodeCopyright("Arsen")]
[NodeToolTip( "Gets an object from a list of GameObjects.")]
[NodeAuthor("Arsen", "---")]
[NodeHelp("Arsen")]

[FriendlyName("Get GameObject From List", "Gets the object at the specified index from a list of GameObjects.")]
public class uScriptAct_GetGameObjectFromList : uScriptLogic
{
	public bool Out { get { return true; } }

	public void In(
		[FriendlyName("List", "The GameObject list.")]
		GameObject [] list,

		[FriendlyName("Index", "The target object index.")]
		int index,

		[FriendlyName("GameObject", "The GameObject.")]
		out GameObject gameObject
	)
	{
		// TODO: There is no range checking here.
		gameObject = list[ index ];
	}
}
